$(function(){
    var dial = $('.remodal').remodal();
    var dial_photo = $('.remodal-photo').remodal();
    $('.menu-item--logout').on('click',function(e){
        confirm('Are you sure?');
         e.stopPropagation();
        e.preventDefault();
    })
    $('.main-form__value__img').on('click',function(e){
        $('.remodal-photo').find('img').attr('src',$(this).attr('data-src'));
       dial_photo.open();
       e.stopPropagation();
        e.preventDefault();
    })
    $('.main-form__submit button').on('click',function(e){
       dial.open();
       e.stopPropagation();
        e.preventDefault();
    })
    $('.main-form__upload').on('click','button',function(e){
        $('.main-form__upload').find('input').click();
        e.stopPropagation();
        e.preventDefault();
    })
     $('.main-form__').on('click','button',function(e){
        $('.main-form__upload').find('input').click();
        e.stopPropagation();
        e.preventDefault();
    })
})

